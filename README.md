<div style="text-align: justify;">


# Rapport Goodenough

## 1. Nom et composition de l'équipe sur la page de garde
|Nom|Equipe|
|-|-|
|DENIEL Amandine|ENGINE|
|GADREY Sophie|ENGINE|
|ROUSSEAUX François|ENGINE|
|DAMOUR Eugène|REST|
|SACHET Boris|REST|
|LE CHENADEC Erwann|TESTS|
|NAVET Benjamin|TESTS|
|WIEDERHOLD Camille|TESTS|

--------------------------
## 1. Contexte du travail

Travail réalisé afin de développer une application web de gestion de réservation pour une cafétéria.

Organisation du travail:

Le travail a été réalisé à distance en quasi-totalité lors des réunions CAOS.

Les outils qui ont été utilisés :
- Plateforme **Teams** pour les appels visios et les réunions
- **Git/GitLab** pour le dépôt du code
- **Intellij** pour la réalisation de développement
- **Code With Me** pour coder en parallèle
- **Postman** pour le test (non systémique) des requêtes REST


Le travail a été rotatif :
L'équipe a été séparée en 3 groupes. Nous avons fait une mise en commun à chaque réunion afin de voir ce qui avait avancé et les difficultés rencontrées pour les résoudre. Des réunions étaient fait en moyenne deux fois par semaine.

Délai de mise en production : 1 mois et demi.

--------------------------
## 2. La liste des User Stories prises en charge

De la liste des demandes présentée par le product owner lors de la rencontre initiale, nous avons pu prendre en charge l'ensemble des user stories présentées, soit :

- **Login** : En tant que client je peux me **connecter** au système cafet afin de l'utiliser et d'obtenir un CID (customerID) et un userName.

- **List**: En tant que client connecté je peux **consulter** la liste des produits afin de faire mon choix.

- **Add**: En tant que client connecté je peux **ajouter un produit** du catalogue dans mon panier, à condition que ce produit soit disponible.

- **Order**: En tant que client connecté, je peux **valider** mon panier

- **Clear**: En tant que client je peux **annuler** mon panier sans commander.
--------------------------
## 3. L'API sous forme de description de requêtes et réponses HTTP

### Login

Obtenir un identifiant d'utilisateur

|Variable|Usage|
|-|-|
`:name`|est le nom de l'utilisateur
`:passwd`|est le mot de passe de l'utilisateur

```
GET /login?name=:name&passwd=:passwd
```

Exemple de requête :
```
GET http://localhost:8080/api/login?name=nemo&passwd=98765
```

Exemple de réponse :

```
{
    "uid":"1234"
}
```

Messages d'erreurs possibles :
|Code|Message|Signification|
|-|-|-|
|404|"Not found"|Couple login / mot de passe invalide|

--------------------------
### List

|Variable|Usage|
|-|-|
`:uid`|identifiant de l'utilisateur retourné par le login

Obtenir les produits (si on est connecté)
```
GET /products?uid=:uid
```
Exemple de requête :
```
GET http://localhost:8080/api/products?uid=1234
```
Exemple de réponse :
```
[
    {
        "pid" : 1,
        "type" : "sandwich",
        "name" : "americain",
        "price": 12.50
    },
    {
        "pid" : 22,
        "type" : "sandwich",
        "name" : "grec",
        "price": 10.0
    },
    {
        "pid" : 3141592,
        "type" : "boisson",
        "name" : "coca",
        "price": 3.0
    }
]
```

Messages d'erreurs possibles :
|Code|Message|Signification|
|-|-|-|
|400|"Bad Request"|La syntaxe de la requête est erronée|
|401|"Unauthorized"|Authentification requise / invalide|

--------------------------
### Add

|Variable|Usage|
|-|-|
`:uid`|identifiant de l'utilisateur retourné par le login
`:pid`|identifiant du produit retourné par le list
`:amount`|quantité de produit à ajouter

Ajouter des produits dans le panier de l'user (si on est connecté)
```
POST /add/:uid?pid=:pid&amount=:amount
```
Exemple de requête :
```
POST http://localhost:8080/api/add/1234?pid=3141592&amount=:6
```

Exemple de réponse OK :

```
{}
```

Messages d'erreurs possibles :
|Code|Message|Signification|
|-|-|-|
|400|"Bad-request"| La syntaxe de la requete est éronnée.
|404|"Not Found"|Le produit demandé n'existe pas / l'uid n'existe pas / Stock insuffisant|

--------------------------
### Order
|Variable|Usage|
|-|-|
`:uid`|identifiant de l'utilisateur retourné par le login

Valider le panier
```
PUT /order/:uid
```
Exemple de requête :
```
PUT http://localhost:8080/api/order/1234
```
Exemple de réponse OK :

```
{}
```
Messages d'erreurs possibles :
|Code|Message|Signification|
|-|-|-|
|400|"Bad Request"|La syntaxe de la requête est erronée|
|404|"Not Found"|Uid n'existe pas|

--------------------------
### Clear
Annuler le panier sans commander

|Variable|Usage|
|-|-|
`:uid`|identifiant de l'utilisateur retourné par le login

Annuler le panier
```l;:
DELETE /clear/:uid
```
Exemple de requête :
```
DELETE http://localhost:8080/api/clear/1234
```

Messages d'erreurs possibles :
|Code|Message|Signification|
|-|-|-|
|400|"Bad-request"|La syntaxe de la requête est erronée
|404|"Not Found"|Uid n'existe pas|

--------------------------
## 4. La structure générale du projet (architecture UML)

(_8(l)
diagramme sur Drawio

--------------------------
## 5. L'approche générale pour la validation (tests)


L'équipe TEST a écrit le squelette des tests REST sur Postman.

Pour tous les tests, nous avons réalisé des :
- Tests unitaires pour vérifier le fonctionnement en conditions normales.
- Tests de robustesse pour vérifier le comportement avec des données erronées et des cas extrêmes.

Version finale :
Pour les tests engine : Junit 5
Pour les tests REST : Framework Jersey test basé sur Junit 4 (grâce à la rétrocompatibilité de Junit 5 via Junit vintage).

Deux catégories de tests : Engine et REST
Un fichier de tests documenté par classe a été rédigé.

--------------------------
## 6. L'organisation : qui a fait quoi avec qui

### Répartition des taches :
#### Groupe Engine :
- Réalisation de la javadoc et prise en compte des spécifications du cahier des charges.
- Implémentation de la partie Engine : 4 classes ont été développées pour le fonctionnement de la cafétéria à partir de 4 interfaces.
    - *CustomerImpl.java*
    - *EnginePhonyImpl.java*
    - *OrderImpl.java*
    - *ProductImpl.java*
- Ajustement de l'interface en cas d'erreur de conception.
- Implémentation des tests Junit 5 pour :
    - *CustomerTest.java*
- Conversion de tests Postman en JerseyTest pour la partie Rest.
    - *LoginTest.java*
    - *ProductsTest.java*


#### Groupe Rest:
- Implémentation de la partie Rest répondant aux user stories.
    - Une classe par user story a cause de la façon dont elles sont définies (utilisation de méthodes http similaires) et pour des raisons de simplicitée de lecture.
        - *Add.java*
        - *Clear.java*
        - *Login.java*
        - *Order.java*
        - *Products.java*
    - Gestion des code d'erreurs et données JSON renvoyées.
- Implémentation des DTO pour les objects customers et products.
    - *CustomerDTO.java*
    - *ProductDTO.java*
- Implémentation des tests Junit 5 pour :
    - *EngineTest.java*
- Conversion de tests Postman en JerseyTest pour la partie Rest.
    - *AddTest.java*

#### Groupe Tests :
- Implémentation de tests Postman pour les différentes actions REST possibles sur notre API basé sur les user stories.
    - *Login*
    - *List*
    - *Add*
    - *Order*
    - *Clear*
- Implémentation de tests sur Junit 5 pour deux classes de l'engine.
    - *OrderTest.java*
    - *ProductTest.java*
- Conversion de tests Postman en JerseyTest pour la partie Rest.
    - *OrderTest.java*
    - *ClearTest.java*

### Outils utilisés :

#### Teams :
- Toutes nos réunions de synchronisation
- Partage de documents.
- Le travail dans les sous équipes.
- Communication / coordination.

#### Git / Gitlab :
- Partage de documents
- Utilisation de plusieurs branches :
    - master : Branche principale, utilisée pour les versions releases et le rendu final.
    - dev : Branche de merge des développements en cours. Utilisée lorsque les implémentations des branches ft sont "stables". Merge effectué depuis cette branche pour les branches ft afin de repartir sur la même version commune.
    - ft-rest : Branche de développement de la partie **Rest**.
    - ft-test : Branche de développement de la partie **Test**.
    - ft-engine : Branche de développement de la partie **Engine**.
- Une fois que les branches ft-rest et ft-engine ont été finalisées, les tests correspondants ont été mergés directement dans ces branches pour simplifier les correctifs.

#### Intellij
- Code principal en java.
- Travail collaboratif avec le plug-in Code withMe.

#### Junit 5
- Outil de test d'intelliJ pour tester l'implémentation de la partie **Engine**.

#### Jersey test
- Framework de test automatique pour jersey, basé sur Junit 4.
- Permet de faire des tests sur l'api en java, et l'intégration des tests api directement permet de vérifier en direct les données dans l'engine pour tester le fonctionnement du front-end et du back-end en même temps.

#### Postman
- Plateforme collaborative permettant de tester l'API.

### Import des données de fonctionnement/test
- Les données de test sont stockés dans deux CSV (un pour les customers, un pour les produits). Qui sont lut et traités en une liste d'objets lors de l'initialisation de l'engine.


--------------------------
## 7. Le bilan : ce qui a marché ou pas, axes de progrès.

Ce qui a marché :
Travail organisé sur différentes équipes, nous faisions des points réguliers à distance afin de mettre en commun nos avancées et aussi là où nous pouvions bloquer. Chacune des équipes avaient sa propre branche sur Git afin de pouvoir avancer simultanément sans attendre le rendu des autres équipes.
Chaque équipe travaillait ensuite grâce au plug-in Code with me qui permet sur IntelliJ de travailler à plusieurs sur le même code.

Difficultés rencontrées :
- S'accorder sur l'implémentation de l'engine.
- Prise en main et découverte de Postman.
- Rédaction de la javadoc pour définir de façon la plus détaillée possible le fonctionnement des méthodes.
- Fonctionnement de l'API (problème de fonctionnement au départ du projet).
- Démarrage du serveur Tomcat.
- Prise en main et découverte des JerseyTest.
- Problème de dépendance lié à Maven.

Axes de progrès :
- Aller plus loin sur les fonctionnalités / implémenter plus d'user stories.
- Réécriture de l'api de façon plus censée, en utilisant des méthodes http différentes. On pourrait mutualiser les fonctionnalitées de liste des produits/add/order/clear sur un seul endpoint api.
- Meilleure organisation des tâches : Utilisation de Trello et des méthodes Agiles de gestion de projet.
- Ajout de synchronized dans la classe product pour gérer les utilisations parallèles du stock (et suppression du getter de stock).

Les tests ont révélé :

- putInStockRobustnessTest(int amount) [classe ProductTest] : test de robustesse visant à ajouter un produit invalide avec une quantité négative -1. La méthode putInStock(int amount) ne faisait pas de contrôle sur les valeurs entrées en paramètre inférieures à 0. Par répercussion, la méthode setAmount() créais une erreur comme elle faisait appel à putInStcok().

- Idem pour takeFromWithLimitedStockRobustness(int amount) [classe ProductTest]:
  La méthode takeFromStock(int amount) ne faisait pas de contrôle sur les valeurs entrées en paramètre inférieures à 0.

- Ces erreurs révélées lors des passages de tests ont permis de mettre en lumière des oublis lors de l'implémentation de engine, notamment la gestion d'amount en quantité négative qui n'avait pas été géré par l'api. L'équipe engine a du revoir certaines méthodes suite à la première phase de test.


</div>
